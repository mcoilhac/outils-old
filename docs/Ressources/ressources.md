---
author: Mireille Coilhac
title: Ressources
---

## Lien vers le site de Nathalie Bessonnet

[Catalogue de ressources](https://natb_nsi.forge.aeif.fr/ressources/){ .md-button target="_blank" rel="noopener" }

## Epreuves pratiques 2024

[thèmes EP](https://mooc-forums.inria.fr/moocnsi/t/bns-2024-qui-a-les-sujets/10981/42){ .md-button target="_blank" rel="noopener" }

[EP 2024 corrections](https://pixees.fr/informatiquelycee/term/index.html#ep_prat){ .md-button target="_blank" rel="noopener" }

[EP 2024 et CodEx framacalc](https://lite.framacalc.org/ep2024_codex-a6mv){ .md-button target="_blank" rel="noopener" }

[Enoncé et corrigé de Laura Fleron sur site interactif](https://lfleron.forge.aeif.fr/nsi_ep2023/){ .md-button target="_blank" rel="noopener" }

[Réponses de Laurent Abbal](https://www.codepuzzle.io/defis-banque){ .md-button target="_blank" rel="noopener" }


## Lien vers les épreuves pratiques 2023 et 2024 ré-écrites par Gilles Lassus

[EP 2023 Gilles Lassus](https://glassus.github.io/terminale_nsi/T6_6_Epreuve_pratique/BNS_2023/){ .md-button target="_blank" rel="noopener" }

[Dépôt Gilles Lassus](https://github.com/glassus/terminale_nsi){ .md-button target="_blank" rel="noopener" }

[EP 2024 Gilles Lassus](https://glassus.github.io/terminale_nsi/T6_6_Epreuve_pratique/BNS_2024/){ .md-button target="_blank" rel="noopener" }



## Dépôt Fabrice Nativel

[Dépôt Fabrice Nativel](https://github.com/fabricenativel/fabricenativel.github.io){ .md-button target="_blank" rel="noopener" }

## Cours de Frédéric Junier en terminale

[Term Frédéric Junier](https://fjunier.forge.apps.education.fr/tnsi/){ .md-button target="_blank" rel="noopener" }


## Dépôts de Romain Janvier

* [Dépôt Term RJ](https://forge.aeif.fr/RomainJanvier/nsiterminale/-/blob/main/mkdocs.yml){ .md-button target="_blank" rel="noopener" }
* [Rendu Term RJ](https://romainjanvier.forge.aeif.fr/nsiterminale/0_revisions_1e/1_parcours_listes_textes/sujet/){ .md-button target="_blank" rel="noopener" }
* [Dépôt 1ere RJ]( https://forge.aeif.fr/RomainJanvier/nsipremiere){ .md-button target="_blank" rel="noopener" }
* [Rendu 1ere RJ](https://romainjanvier.forge.aeif.fr/nsipremiere/){ .md-button target="_blank" rel="noopener" }

## Lumni

[Lumni](https://www.lumni.fr/lycee/terminale/nsi){ .md-button target="_blank" rel="noopener" }


## Livre IA

[Livre IA](https://www.ai4t.eu/book/ia-pour-les-enseignants--un-manuel-ouvert-1/index?path=index){ .md-button target="_blank" rel="noopener" }

## Eduscol NSI

[Eduscol NSI](https://eduscol.education.fr/2068/programmes-et-ressources-en-numerique-et-sciences-informatiques-voie-g){ .md-button target="_blank" rel="noopener" }

[Eduscol SNT et NSI vademecum](https://eduscol.education.fr/1670/programmes-et-ressources-en-sciences-numeriques-et-technologie-voie-gt){ .md-button target="_blank" rel="noopener" }

## Boutons de liens

[doc officielle boutons](https://squidfunk.github.io/mkdocs-material/reference/buttons/){ .md-button target="_blank" rel="noopener" }

## Lien collègue maths/NSI : 

[Luc COURBON](https://lycee-maths.info/){ .md-button target="_blank" rel="noopener" }

##  Code puzzle Laurent ABBAL

[Code Puzzle](https://www.codepuzzle.io/devoir-creer){ .md-button target="_blank" rel="noopener" }

[DOC Code Puzzle](https://codepuzzle-io.github.io/documentation/04-entrainements-devoirs/){ .md-button target="_blank" rel="noopener" }



## Sujets de bac et corrections


[Le web péda annales NSI](https://lewebpedagogique.com/dlaporte/){ .md-button target="_blank" rel="noopener" }


## Partage de DS Nicolas Romain Mireille

[professeur_exemple_essais](https://forge.aeif.fr/professeur_exemple/essais){ .md-button target="_blank" rel="noopener" }


## Dépots de DS de Romain

[Depot Romain 1ère 2022-2023](https://www.dropbox.com/scl/fo/48mkidaghz6xan2r7leqj/h?rlkey=y6xl17sh7p2ayisw5nhjmbnbp&dl=0){ .md-button target="_blank" rel="noopener" }
[Depot Romain 1ère 2023-2024](https://www.dropbox.com/scl/fo/643w4ekb3e7npaq16utfd/h?rlkey=if026n9y9s4p372qtijwrm9jw&dl=0){ .md-button target="_blank" rel="noopener" }
[Depot Romain Term 2022-2023](https://www.dropbox.com/scl/fo/ta50xrv7hn97tzvj4f63o/h?rlkey=7f0jr2xt6e8iaodn6h6yq16te&dl=0){ .md-button target="_blank" rel="noopener" }
[Depot Romain Term 2023-2024](https://www.dropbox.com/scl/fo/5xdnet39e2iz99023vw0j/h?rlkey=n798b55wizxwk03sj481vxl5h&dl=0){ .md-button target="_blank" rel="noopener" }

## Graphes

[Algo Graphe en ligne](https://frederic-zinelli.gitlab.io/algographe/){ .md-button target="_blank" rel="noopener" }
[Doc Algo graphe](https://frederic-zinelli.gitlab.io/algographe/docs/){ .md-button target="_blank" rel="noopener" }
[Téléchargement version 1.3.1](https://gitlab.com/frederic-zinelli/algographe){ .md-button target="_blank" rel="noopener" }

## Site 1ère de Charles

[Site première Charles](https://cours-nsi.forge.apps.education.fr/premiere/introduction.html){ .md-button target="_blank" rel="noopener" }

## Enigmes de Nicolas algo et fractions

[Dépot énigmes](https://forge.aeif.fr/nreveret/enigmes/){ .md-button target="_blank" rel="noopener" }   
[Enigmes](https://nreveret.forge.aeif.fr/enigmes/){ .md-button target="_blank" rel="noopener" }
[Dépot fractions](https://forge.apps.education.fr/nreveret/visualisation_fractions){ .md-button target="_blank" rel="noopener" }
[Fractions](https://nreveret.forge.apps.education.fr/visualisation_fractions/){ .md-button target="_blank" rel="noopener" }

## Découpage de fichiers de Romain Janvier

[Découpage de fichiers](https://forge.aeif.fr/RomainJanvier/outils-divers/-/tree/main/decoupe_fichiers?ref_type=heads){ .md-button target="_blank" rel="noopener" }

## Epreuve pratique 2023 sujets et corrections

[BNS 2023](https://pixees.fr/informatiquelycee/term/ep/index.html){ .md-button target="_blank" rel="noopener" }

## Site terminale frédéric Junier

[Dépot Term Frédéric Junier](https://forge.aeif.fr/fjunier/terminale_nsi/){ .md-button target="_blank" rel="noopener" }  
[Term Frédéric Junier](https://fjunier.forge.aeif.fr/terminale_nsi/){ .md-button target="_blank" rel="noopener" }

## Vidéos MOOC NSI

[Vidéos MOOC NSI](https://www.youtube.com/@moocnsi){ .md-button target="_blank" rel="noopener" }

## Mermaid Franck

[Mermaid Franck](https://pratique.forge.apps.education.fr/arbre/1-arbre-bin/40-dessin/#bac-a-sable){ .md-button target="_blank" rel="noopener" }