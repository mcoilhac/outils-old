---
author: Mireille Coilhac
title: Mise à jour Pyodide-MkDocs 0.9.0
---

## Mise à jour Pyodide-MkDocs 0.9.0


## Dans le dossier `my_theme_customizations`

👉 Remplacer l'ancien dossier par le nouveau à télécharger ici : 

🌐  `my_theme_customizations.zip` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/my_theme_customizations.zip)

Attention, par rapport à la version de Vincent Bouillot, les fichiers start_REM.md et end_REM.md ont été modifiés (A et Z remplacés par un espace)

⚠️ Ne pas oublier de rajouter le dossier qcm


## A la racine

Remplacer les fichiers `main.py`, par le nouveau :

🌐  `main.zip` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/main.zip)

👉 Dans `mkdocs.yml` : avoir la ligne `- ... | regex=^(?:(?!_REM.md).)*$`

```md
nav:
  - "🏡 Accueil": index.md
  - ... | regex=^(?:(?!_REM.md).)*$

```

👉 Dans `requirements.txt` 

La première ligne doit être : 

```txt
mkdocs-material>=8,<9
```

## Les syntaxes à utiliser

[MAJ Pyodide janvier 2023](https://bouillotvincent.gitlab.io/pyodide-mkdocs/){ .md-button target="_blank" rel="noopener" }

!!! warning "Pour que les `assert` soient évalués"

    Pour que les assert fonctionnent (version 0.9.0):

    * Mettre un message dans l'assert
    * Ou écrire la ligne `# Tests` au dessus


!!! warning "`###` en haut à droite de la fenêtre d'exercice (IDE)"

    * `###` n'apparaît que si la ligne `# Tests` est présente.
    * Si on clique sur `###`toute les lignes en dessous de `# Tests` sont passées en commentaire. 
    * On peut évidemment cliquer à nouveau dessus pour supprimer cette mise en commentaire.


!!! warning "Décomptage du nombe d'essais par l'élève"

    Si les `assert` présents dans l'énoncé "ne passent pas" le décomptage est bloqué.

    👉 Dire aux élèves de cliquer sur `###` situé en haut à droite de la fenêtre avant les validations pour que le décomptage fonctionne.

