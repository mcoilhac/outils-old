---
author: Mireille Coilhac
title: Mise à jour Pyodide-MkDocs 0.9.1
---

## Mise à jour Pyodide-MkDocs 0.9.1

## Dans le dossier `my_theme_customizations`



🌐: ["Site Vincent Bouillot"](https://bouillotvincent.gitlab.io/pyodide-mkdocs/#apercu)

Attention, par rapport à la version de Vincent Bouillot, les fichiers start_REM.md et end_REM.md ont été modifiés (A et Z remplacés par un espace)

* modifier main.html avec celui téléchargé

### Dans pyodide-mkdocs 

* modifier ide.js avec celui téléchargé
* modifier interpreter.js avec celui téléchargé
* télécharger utils.js 

## A la racine

Remplacer les fichiers `main.py`, par le nouveau : mars_2023



## Les syntaxes à utiliser


!!! warning "`###` en haut à droite de la fenêtre d'exercice (IDE)"

    * `###` n'apparaît que si la ligne `# Tests` est présente.
    * Si on clique sur `###`toutes les lignes en dessous de `# Tests` sont passées en commentaire. 
    * On peut évidemment cliquer à nouveau dessus pour supprimer cette mise en commentaire.


!!! warning "Décomptage du nombe d'essais par l'élève"

    Si les `assert` présents dans l'énoncé "ne passent pas" le décomptage est bloqué.

    👉 Dire aux élèves de cliquer sur `###` situé en haut à droite de la fenêtre avant les validations pour que le décomptage fonctionne.

