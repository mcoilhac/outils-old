---
author: Mireille Coilhac
title: Images gif
---

Créer un gif à partir de plusieurs fichiers png : 

[cleverpdf](https://www.cleverpdf.com/fr/gif-maker){ .md-button target="_blank" rel="noopener" }

Télécharger l'image, et l'tiliser de façon classique :

![circuit gif](images/circuit.gif){ width=40% }

![pas circuit gif](images/pas_circuit.gif){ width=40% }
